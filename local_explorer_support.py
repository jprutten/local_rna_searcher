#mutates a sequence with a given mutation at a given location
def mutate(startsequence,mutation, mutationnr): 
	mutatedsequence = ""
	for nr in range(mutationnr):
		mutatedsequence=mutatedsequence+str(startsequence[nr])
	mutatedsequence=mutatedsequence+str(mutation)
	for nr in range(mutationnr+1,len(startsequence)):
		mutatedsequence=mutatedsequence+str(startsequence[nr])
	return mutatedsequence

# makes a copy of a list so that changes in the copy don't change the original
def copylist(original): 
    copy = list()
    for item in original:
        copy.append(item)
    return copy

# returns a list comprised of the two initial lists.
#The entries of the second list are placed after the entries of the first list
def addlist(firstlist,secondlist):
    totallist = list()
    for item in firstlist:
        totallist.append(item)
    for item in secondlist:
        totallist.append(item)
    return(totallist)
        
#returns the number mutations required to obtain sequence 1 out of sequence 2
def distancesequence(sequence1,sequence2):
    distance = 0
    for basenr in range(len(sequence1)):
        if sequence1[basenr] not in sequence2[basenr]:
            distance+=1
    return distance


#returns the lineage of sequence the mutant has been created by. Any mutant that 
def restructlineage(sequence,mutantnrs,mutantlist):
    lineagelist= list()
    lineagelist.append(sequence)
    currentform = sequence
    for mutation in range(len(mutantnrs)):
        currentform = mutate(currentform,mutantlist[mutation],mutantnrs[mutation])
        lineagelist.append(currentform)
    return lineagelist

def properbased(sequence):
	returnsequence = ""
	for letter in sequence:
		if letter in "AUGC":
			returnsequence = returnsequence+letter
		elif letter in "T":
			returnsequence= returnsequence + "U"
	return returnsequence

def listprint(network,writelocation):
	f = open(writelocation+".sif", 'w')
	for itemnr in range(len(network)):
		f.write(network[itemnr][0]+'\n')
	f.close()

def sifprint(network,writelocation,fullsequence=0):
	f = open(writelocation+".sif", 'w')
	for itemnr in range(len(network)):
		for item in network[itemnr][1]:
			if fullsequence:
				f.write(network[itemnr][0]+" s1 "+network[item][0]+'\n')
			else:
				f.write(str(itemnr)+" s1 "+str(item)+'\n')
	f.close()
