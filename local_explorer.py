import sys
#change this to the directory that holds the interfaces
RNAfoldingDirectory = "/home/bioinf/Documents/local_search/ViennaRNA-2.4.9/"
# makes sure the script automatically imports the right RNAfolder
if sys.version_info.major is 3:
	interface = "interfaces/Python3"
elif sys.version_info.major is 2:
	interface = "interfaces/Python"
print(RNAfoldingDirectory+interface)
sys.path.append(RNAfoldingDirectory+interface)

import RNA
import local_explorer_support as les







#check if new mutant is a one step mutant of any of the previous steps, in which case it will already have been considered at a lower nr of mutations
def checkloop(sequence,mutantsequence,mutantnrs,mutantlist): 
	lineage = les.restructlineage(sequence,mutantnrs,mutantlist)
	newmutant=True
	for lineagetime in range(len(lineage)-2):
		#print('lt',lineagetime,mutantsequence,lineage[lineagetime],distancesequence(mutantsequence,lineage[lineagetime]))
		if( les.distancesequence(mutantsequence,lineage[lineagetime]) <2):
			newmutant=False
	return newmutant

#recursive function that takes a given sequence and checks all its 1 steps mutants to a given fold.
#This function returns a the number of successful folds it encountered, the number of unsuccesful folds it tried,
#and the list of sequences that do fold in the same way as the initial sequence.
#This list can contain duplicates when checked at depths greater than 2. These need to be pruned out.

#If searchlength is smaller than currentdepth we want to run the current level of mutant step,
#if searchlength is smaller than currentdepth+1, we want to search even deeper, so we call findmutants again at a deeper recursive level
#Since we're only interested in the neutral network, we only go a deeper level with sequences that fold according to our fold of interest
#Several checks have been built in to prevent threading the same ground multiple times:
#	We do not allow a mutation that keeps the sequence the same, as it is not a mutation at all
#	We do not allow the same position to be mutated twice in succession, as that is space already explored one layer earlier
#	We check the lineage of mutations a sequence has undergone to see if it's 1 step mutational neighborhood contains that mutant already. These mutations are not checked again
def findmutants(sequence,checkfold,initialenergy,maxdeviation,searchlength,currentdepth=0,mutantnrs=0,mutantlist=0,initialsequence = 0):
	successes = 0
	fails = 0
	successlist = list()
	if currentdepth==0:
		initialsequence = sequence #initialsequence required to reconstruct the mutational lineage from
		successlist.append(initialsequence)
	#if currentdepth > 2:
	#	print(currentdepth,sequence)
	newmutantnrs = list()
	newmutantlist = list()
	if mutantnrs !=0: #this makes sure that you can start the method with an empty list but give subsequent mutant steps the previous steps so you do not double back on yourself without at least one additional mutation 
		newmutantnrs = mutantnrs
	if mutantlist !=0: #this makes sure that you can start the method with an empty list but give subsequent mutant steps the previous steps so you do not double back on yourself without at least one additional mutation 
		newmutantlist = mutantlist	   
	for base,basenr in zip(sequence,range(len(sequence))):
		for mutation in ["A","C","G","U"]:
			if base not in mutation:
				mutantsequence = les.mutate(sequence,mutation,basenr)
				currentmutantnrs = les.copylist(newmutantnrs)
				currentmutantlist = les.copylist(newmutantlist)
				currentmutantnrs.append(basenr)
				currentmutantlist.append(mutation)
				if ((len(newmutantlist)==0 or newmutantnrs[-1]!=basenr) and 
					(currentdepth<2 or checkloop(initialsequence,mutantsequence,currentmutantnrs,currentmutantlist))):
					mutantfold=RNA.fold(str(mutantsequence))
					if (mutantfold[0] == checkfold and (maxdeviation ==0. or mutantfold[1]<=initialenergy*maxdeviation)):
						successes+=1
						successlist.append(mutantsequence)
						if currentdepth+1<searchlength:
							[depthsuccesses,depthfails,newsuccesslist] =findmutants(mutantsequence,checkfold,initialenergy,maxdeviation,searchlength,currentdepth+1,currentmutantnrs,currentmutantlist,initialsequence)
							successes+=depthsuccesses
							fails+=depthfails
							successlist = les.addlist(successlist,newsuccesslist)
					else:
						fails +=1
	return([successes,fails,successlist])

#function prunes network for duplicate entries and connects all 1 step mutants in an array (currently one way)

def ordernetwork(OGnetwork):
	network = les.copylist(OGnetwork)
	itemnr = 0
	totalnetwork = list()
	
	#pruning the network
	while itemnr < len(network):
		pastitem = itemnr+1

		while pastitem < len(network):
			distance = les.distancesequence(network[itemnr],network[pastitem])
			if distance == 0: #sequences are identical, so we can remove the second one from the network
				del network[pastitem]
				pastitem -=1
			pastitem+=1
		itemnr+=1
	#checking 1 distance connections
	for itemnr in range(len(network)):
		localnetwork = list()
		for pastitem in range(itemnr+1,len(network)):
			distance = les.distancesequence(network[itemnr],network[pastitem])
			if distance == 1: #sequences are one step mutants mutants 
				localnetwork.append(pastitem)
		totalnetwork.append([network[itemnr],localnetwork])
		itemnr+=1
	return totalnetwork



def successlist(sequence,MFEreductionrange,searchdepth,initialenergy=0,initialfold= ""): #call this function from a scrip to have the list of folds and their network you
	initialsequence = les.properbased(sequence) #switches out T bases and replaces them with U
	if not initialfold:
		initialfold = RNA.fold(initialsequence)[0] 
	if (initialenergy == 0):
		initialenergy = RNA.fold(initialsequence)[1]
	successlist= findmutants (initialsequence,initialfold,initialenergy,MFEreductionrange,searchdepth)[2]
	return(ordernetwork(successlist))

if __name__ == "__main__": #this is the where the script will start when called from the terminal

	
	initialsequence = sys.argv[1] # the starting sequence
	MFEreductionrange = 0.# the reduction of energy allowed to occur within the fold. Default value 0 means it is not taken into account
	if len(sys.argv)>2:
		MFEreductionrange = float(sys.argv[2])
	searchdepth = 1 #the nr of step mutants you do, defaults to 1
	if len(sys.argv)>3: 
		searchdepth = int(sys.argv[3])
	print(initialsequence,MFEreductionrange,searchdepth)
	finalnetwork= successlist(initialsequence, MFEreductionrange,searchdepth)
	les.listprint(network,"seqlist")
	les.sifprint(finalnetwork,"./fullsequences",1)
	#print(finalnetwork) # Can also be written to file easily, or you can call the script from a script
